# TEACH ME SHAPES #

Assignment 2 of MAD5314-APPLICATION FOR WEARABLE DEVICES.

### Introduction ###

* The purpose of this activity is to build an education app to teach autistic children about shapes.
* For that will be used an iOS App (iPhone) + Particle App (Internet Button)

### Description/Requirements ###

The app consists of 3 types of learning activities:


Description of activity

#### MANDATORY ####
* Learning Activity: Identifying number of sides in a shape
- Phone shows a shape
- Phone asks: “How many sides does this shape have?”
- Child uses the Particle’s buttons to indicate how many sides the shape has
- Phone displays if the answer is CORRECT or INCORRECT.
- Particle displays if answer is CORRECT or INCORRECT
- ##### Other requirements: #####
- Please implement minimum 2 different shapes.

#### OPTION A ####
* Drawing a shape: Particle shows a shape using its LEDs
    - Example: 
        - Circle
        - Square (make 4 lights turn on in a square-ish shape)
        - Triangle (make 3 lights turn on in a triangle-ish shape)

* Child needs to DRAW the shape on the Phone
    - Phone displays if answer is CORRECT or INCORRECT
    - Particle displays if answer is CORRECT or INCORRECT
    - Other requirements:
        - Please use an AI library to determine the correctness of the drawn shape. If you are unable to use an AI library, then you may use a Gesture Recognizer to detect the shapes
        - Please implement minimum 2 different shapes

#### OPTION B ####
* Rotating a shape: 
    - Phone shows a shape
    - Child rotates the Particle
    - Phone responds by rotating the displayed shape the same amount the Particle was rotated.
    - Example - if Particle was rotated 20 degrees, then rotate the shape by 20 degrees
* Other requirements:
    - Please implement this with an interesting shape (eg: NOT A CIRCLE!)
    - Bonus mark if you are able to implement rotation with a 3D shape.


### App Requirements ###

* #### R1: Two (2) of the three activities should be part of the same app. ####
    - You must implement LO1
    - Choose either Option A or Option B to implement

* #### R2: The app should give the child a way to select what activity they want to play. ####

* #### R3: There should be a “random” mode. This mode will randomly select an activity for the child to complete. ####

* #### R4:  The system should keep track of how many questions the child answered correctly. The child can view their score by pushing a button on the Particle.  After pushing the button, The score is shown on the phone and Particle. ####


### AUTHOR ###

* Antonio Merendaz do Carmo Neto
* C0741427