// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>
#include "math.h"

/* Did you know that the Internet Button can detect if it's moving? It's true!
Specifically it can read when it's being accelerated. Recall that gravity
is a constant acceleration and this becomes very useful- you know the orientation!*/

InternetButton b = InternetButton();
int ledPos = 0;
int transZ = 0;
String numSides = "0";
String pubLedPos = "0";

void setup() {
    // Tell b to get everything ready to go
    // Use b.begin(1); if you have the original SparkButton, which does not have a buzzer or a plastic enclosure
    // to use, just add a '1' between the parentheses in the code below.
    b.begin();
    
    //ADD THIS
    Particle.function("answer", showCorrectOrIncorrect);

    // reduce to less than full eye-blazing brightness
    b.setBrightness(45);

    Particle.variable("ledPos", ledPos);
    if (transZ) {
        pubLedPos = String(ledPos) + "-" + String(transZ) +  "-" + numSides;
    }
    // Particle.publish("ledPos", pubLedPos , 60, PRIVATE);
}

void loop(){
    
    
    // previous LED off (or 'null' LED0 off the first time through)
    b.ledOn(ledPos, 0, 0, 0);

    // Want to figure out which LED is the lowest?
    // We've hidden the necessary trigonometry in this function.
    ledPos = b.lowestLed();
    
    // Z position:
    transZ = b.readZ();
    
     if (b.buttonOn(1)) {
          numSides = "3";   
          Particle.publish("ledChoice","3", 60, PUBLIC);
        pubLedPos = String(ledPos) + "-" + String(transZ) +  "-" + numSides;
        Particle.publish("ledPos", pubLedPos , 60, PRIVATE);
        delay(350);
        }
    else if (b.buttonOn(2)) {
        numSides = "4";
        pubLedPos = String(ledPos) + "-" + String(transZ) +  "-" + numSides;
        Particle.publish("ledPos", pubLedPos , 60, PRIVATE);
        Particle.publish("ledChoice","4", 60, PUBLIC);
        delay(350);
     } 
    else if (b.buttonOn(3)) {
        numSides = "5";
        pubLedPos = String(ledPos) + "-" + String(transZ) +  "-" + numSides;
        Particle.publish("ledPos", pubLedPos , 60, PRIVATE);
        Particle.publish("ledChoice","5", 60, PUBLIC);
        delay(350);
    }
    else if (b.buttonOn(4)) {
        numSides = "6";
        pubLedPos = String(ledPos) + "-" + String(transZ) +  "-" + numSides;
        Particle.publish("ledPos", pubLedPos , 60, PRIVATE);
        Particle.publish("ledChoice","6", 60, PUBLIC);
        delay(350);
    }
    
     // pubLedPos = String(ledPos);
    pubLedPos = String(ledPos) + "-" + String(transZ) +  "-" + numSides;
    Particle.publish("ledPos", pubLedPos , 60, PRIVATE);
    
    // give some time for human retinal response
    delay(330);
    // Now turn the lowest LED on
    b.ledOn(ledPos, 0, 30, 30);

    // Wait a mo'
    delay(1000);
}

int showCorrectOrIncorrect(String cmd) {
    // b.allLedsOff();
    // delay(500);
  if (cmd == "green") {
    b.allLedsOn(0,255,0);
    delay(1000);
    b.allLedsOff();
    delay(250);
  }
  else if (cmd == "red") {
    b.allLedsOn(255,0,0);
    delay(1000);
    b.allLedsOff();
    delay(250);
  }
  else {
    // you received an invalid color, so
    // return error code = -1
    return -1;
  }
  // function succesfully finished
  return 1;
}