//
//  ViewController.swift
//  TeachMeShapes
//
//  Created by Antonio Merendaz do Carmo Nt on 2019-11-04.
//  Copyright © 2019 Medtouch. All rights reserved.
//

import UIKit
import Particle_SDK

class ViewController: UIViewController {

    @IBOutlet weak var shapeImgView: UIImageView!
    @IBOutlet weak var answerLbl: UILabel!
    @IBOutlet weak var InstructionsLbl: UILabel!
    var shapes = ["3Sides1", "3Sides2", "3Sides3", "3Sides4", "3Sides5", "4Sides1", "4Sides2", "4Sides3", "4Sides4", "4Sides5", "5Sides1", "5Sides2", "5Sides3", "5Sides4", "5Sides5", "6Sides1", "6Sides2", "6Sides3", "6Sides4", "6Sides5"]
    var img = ""
    var initPosX : CGFloat = 20
    var initPosY : CGFloat = 240
    
    // MARK: User variables
    let USERNAME = "merendaz@gmail.com"
    let PASSWORD = "Nantme2019;"
    
    // MARK: Device
    let DEVICE_ID = "390031000f47363333343437"
    var myPhoton : ParticleDevice?
    
    // MARK: Other variables
    var questionNumber = 0
    var gameScore: Int = 0
    var particleEvent: String = "ledPos"
    var handler : Any?
    var stepSizeX: CGFloat = 0
    var stepSizeZ: CGFloat = 0
    var numSides: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Teach Me Shapes"
        self.answerLbl.text = ""
        self.InstructionsLbl.numberOfLines = 0
        self.answerLbl.numberOfLines = 0
        self.InstructionsLbl.text = "Answer using the Particle Device:\n- Button 1 for 3 Sides\n- Button 2 for 4 Sides\n- Button 3 for 5 Sides \n- Button 4 for 6 Sides"
        self.randImg()
        // 1. Initialize the SDK
       _ =  ParticleCloud.init()
        // 2. Login to your account
        ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
            if (error != nil) {
                // Something went wrong!
                print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                // Print out more detailed information
                print(error?.localizedDescription ?? "")
            }
            else {
//                print("Login success!")
                // try to get the device
                self.getDeviceFromCloud()
            }
        } // end login
    }
    // MARK: Get Device from Cloud
    // Gets the device from the Particle Cloud
    // and sets the global device variable
    func getDeviceFromCloud() {
        ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
            
            if (error != nil) {
                print("Could not get device")
                print(error?.localizedDescription ?? " ")
                return
            }
            else {
//                print("Got photon from cloud: \(device?.id ?? "")")
                self.myPhoton = device
                
                // subscribe to events
                self.subscribeToParticleEvents(event: self.particleEvent)
            }
            
        } // end getDevice()
    }
    
    //MARK: Subscribe to "ledPos" events on Particle
    func subscribeToParticleEvents(event: String) {
        self.handler = ParticleCloud.sharedInstance().subscribeToAllEvents(withPrefix: event, handler: {
            (event :ParticleEvent?, error : Error?) in
            if let _ = error {
                print("could not subscribe to events")
            } else {
                guard let data = event?.data! else { return }
                let arrayData = data.components(separatedBy: "-")
                self.numSides = arrayData[2]
                let charImg = Array(self.img)
                
                let dataintX = -1*(Int(arrayData[0])!-6)
                self.stepSizeX = CGFloat(dataintX) * .pi/6
                var dataintZ = 0
                if (arrayData[1] != "") {
                    dataintZ = -1*(Int(arrayData[1])!)
                }
                else {
                    dataintZ = -1*(Int(arrayData[2])!)
                }
                self.stepSizeZ = CGFloat(dataintZ-30) * .pi/(6*5)
                DispatchQueue.main.async {
                    
                    if (self.numSides == "3"||self.numSides == "4"||self.numSides == "5"||self.numSides == "6") {
                        if (self.numSides == String(charImg[0])) {
                            print("CORRECT!!")
                            self.answerLbl.text = "Correct Answer! = " + self.numSides + " sides"
                            self.turnParticleGreen()
                            self.numSides = "1"
                        } else {
                            print("WRONG!!")
                            self.answerLbl.text = "Wrong Answer! = \n" + "The shape has " + String(charImg[0]) + " sides"
                            self.turnParticleRed()
                            self.numSides = "0"
                        }
                    } else {
                        self.answerLbl.text = "\(self.numSides) => Waiting your choice on Device!"
                    }
                    
                    UIView.animate(withDuration: 1.0, delay: 0, options: .curveEaseIn, animations: {
                    self.shapeImgView.transform = CGAffineTransform(rotationAngle: self.stepSizeX)
//                    self.shapeImgView.layer.transform = CATransform3DMakeRotation(self.stepSizeZ, 0.01, 0, 0)
                    }, completion: nil)
                }
            }
        })
    }
    
    func turnParticleGreen() {
        
        print("Pressed the change lights button")
        
        let parameters = ["green"]
        var task = myPhoton!.callFunction("answer", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to turn green")
            }
            else {
                print("Error when telling Particle to turn green")
            }
        }
        //var bytesToReceive : Int64 = task.countOfBytesExpectedToReceive
        
    }
    
    func turnParticleRed() {
        
        print("Pressed the change lights button")
        
        let parameters = ["red"]
        var task = myPhoton!.callFunction("answer", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to turn red")
            }
            else {
                print("Error when telling Particle to turn red")
            }
        }
        //var bytesToReceive : Int64 = task.countOfBytesExpectedToReceive
    }

    func randImg() {
        let randIndex = Int.random(in: 0...(shapes.count-1))
        self.img = shapes[randIndex]
        self.shapeImgView.image = UIImage.init(named: self.img)
    }

    @IBAction func anotherImgBtnPressed(_ sender: UIButton) {
        self.randImg()
        self.answerLbl.text = ""
        self.shapeImgView.transform = .identity
    }
    
    @IBAction func showAnswerBtnPressed(_ sender: UIButton) {
        let charImg = Array(self.img)
        self.answerLbl.text = "The Shape has \(charImg[0]) sides"
    }
}

